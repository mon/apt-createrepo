NAME = apt-createrepo
VERSION = 0

default:

install:
	install -D apt-createrepo $(DESTDIR)/usr/bin

archive:
	@git archive --prefix=$(NAME)-$(VERSION)/ HEAD --format=tar.gz -o $(NAME)-$(VERSION).tar.gz
	@echo "$(NAME)-$(VERSION).tar.gz created"

clean:
	rm -f *~ *tar.gz
